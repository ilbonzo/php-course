<?php
use Slim\Http\Request;
use Slim\Http\Response;

// Routes
$app->get('/', function (Request $request, Response $response, array $args) {
    $session = new Session();
    $session->redirectIfLogged();
    return $this->renderer->render($response, 'index.phtml', $args);
});

require_once('routes/users.php');

require_once('routes/posts.php');

require_once('routes/films.php');