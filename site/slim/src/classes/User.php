<?php 

Class User {
    private $_id = null;

    private $_email;

    private $_password;

    private $_name = null;

    private $_surname = null;

    public function __construct($email, $password) {
        $this->_email = $email;
        $this->_password = $password;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function getPassword() {
        return $this->_password;
    }

    public function getId() {
        return $this->_id;
    }

    public function getName() {
        return $this->_name;
    }

    public function getSurname() {
        return $this->_surname;
    }

    public function setEmail($value) {
        $this->_email = $value;
    }

    public function setPassword($value) {
        $this->_password = $value;
    }

    public function setId($value) {
        $this->_id = $value;
    }

    public function setName($value) {
        $this->_name = $value;
    }

    public function setSurname($value) {
        $this->_surname = $value;
    }

    public function getEncryptedPassword() {
        $pwdLength = strlen($this->_password);
        $asterisks = str_repeat('*', $pwdLength - 2);
        $pwdEncrypted = $this->_password[0] . $asterisks . $this->_password[$pwdLength - 1];
        return $pwdEncrypted;
    }

    public function getEncryptedPassword2() {
        $pwdLength = strlen($this->_password);
        $pwdEncrypted = '';
        $arrayPassword = str_split($this->_password);

        foreach($arrayPassword as $k => $letter) {
            if ($k === 0 || $k === $pwdLength - 1) {
                $pwdEncrypted .= $letter;
                continue;
            }
            $pwdEncrypted .= '*';
        }
        return $pwdEncrypted;
    }

    private $_dbConnection = null;

    private function _getDbConnection() {
        if (is_null($this->_dbConnection)) {
            $userDb = 'root';
            $passwordDb = 'password';
            $this->_dbConnection = new PDO('mysql:host=localhost;dbname=cms', $userDb, $passwordDb);
        }
        return $this->_dbConnection;
    }

    public function insert() {
        $dbConnection = $this->_getDbConnection();
        $sql = "INSERT INTO users (mail, password, name, surname) VALUES (:mail, :password, :name, :surname)";
        $statement = $dbConnection->prepare($sql);
        $statement->bindParam(':mail', $this->_email, PDO::PARAM_STR);
        $statement->bindParam(':password', $this->_password, PDO::PARAM_STR);
        $statement->bindParam(':name', $this->_name, PDO::PARAM_STR);
        $statement->bindParam(':surname', $this->_surname, PDO::PARAM_STR);
        return $statement->execute();
    }

    public function delete() {
        $dbConnection = $this->_getDbConnection();
        $sql = "DELETE FROM users WHERE id_user = :id";
        $statement = $dbConnection->prepare($sql);
        $statement->bindParam(':id', $this->_id, PDO::PARAM_INT);
        return $statement->execute();
    }

    public function update() {
        $dbConnection = $this->_getDbConnection();
        $sql = "UPDATE users SET mail = :mail, password = :password, name = :name, surname = :surname WHERE id_user = :id";
        $statement = $dbConnection->prepare($sql);
        $statement->bindParam(':id', $this->_id, PDO::PARAM_INT);
        $statement->bindParam(':mail', $this->_email, PDO::PARAM_STR);
        $statement->bindParam(':password', $this->_password, PDO::PARAM_STR);
        $statement->bindParam(':name', $this->_name, PDO::PARAM_STR);
        $statement->bindParam(':surname', $this->_surname, PDO::PARAM_STR);
        return $statement->execute();
    }
    
}