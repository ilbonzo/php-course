<?php

Class Session {
    public function isLogged() {
        if ($_SESSION['logged'] && !$this->sessionExpired($_SESSION['loginDate'])) {
            return true;
        }
        $this->logout();
        return false;
    }

    public function sessionExpired($time) {
        $timeToExpire = $time + (3600 * 24);
        $now = time();
        if ($now > $timeToExpire) {
            return true;
        }
        return false;
    }

    public function logout() {
        $_SESSION['logged'] = false;
        $_SESSION['loginDate'] = null;
    }

    public function login($data) {
        $_SESSION['logged'] = true;
        $_SESSION['loginDate'] = time();
        $_SESSION['email'] = $data['email'];
        $_SESSION['password'] = $data['password'];
        header('Location: /slim/public/users');
        exit();
    }

    public function redirectIfNotLogged() {
        if (!$this->isLogged()) {
            header('Location: /slim/public/');
            exit();
        }
    }

    public function redirectIfLogged() {
        if ($this->isLogged()) {
            header('Location: /slim/public/users');
            exit();
        }
    }
}