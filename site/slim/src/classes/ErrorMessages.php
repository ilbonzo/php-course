<?php

class ErrorMessages {

    public static function initialize() {
        if (!isset($_SESSION['messages'])) {
            self::reset();
        }
    }

    public static function addMessage($message) {
        $_SESSION['messages'][] = $message;
        
    }

    public static function reset() {
        $_SESSION['messages'] = [];
    }

    public static function getMessages() {
        return $_SESSION['messages'];
    }

}