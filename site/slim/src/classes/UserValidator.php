<?php

Class UserValidator {

    public function isValid(User $user) {
        $message = [];
        if (!$this->isEmailValid($user->getEmail())) {
            $message[] = 'error username';
        }
        if (!$this->isNewEmail($user->getEmail())) {
            $message[] = 'error exists';
        }
        if (!$this->isPasswordValid($user->getPassword())) {
            $message[] = 'error password';
        }

        if (count($message) > 0) {
            return $message;
        }
        return true;
    }

    private function isEmailValid($email) {
        return filter_var($email, FILTER_VALIDATE_EMAIL);
    }

    private function isPasswordValid($password) {
        if (strlen($password) < 8) {
            return false;
        }
        return true;
    }

    private function isNewEmail($email) {
        $userCollection = new UserCollection();
        $user = $userCollection->getUserByMail($email);
        if (!$user) {
            return true;
        }
        return false;
    }

}