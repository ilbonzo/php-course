<?php

Class UserCollection {

    private $_dbConnection = null;

    private function _getDbConnection() {
        if (is_null($this->_dbConnection)) {
            $userDb = 'root';
            $passwordDb = 'password';
            $this->_dbConnection = new PDO('mysql:host=localhost;dbname=cms', $userDb, $passwordDb);
        }
        return $this->_dbConnection;
    }

    private function _loadUser($row) {
        if (!$row) {
            return false;
        }
        $u = new User($row['mail'], $row['password']);
        $u->setId($row['id_user']);
        $u->setName($row['name']);
        $u->setSurname($row['surname']);
        return $u;
    }

    public function getUsers() {
        $users = [];
        $dbConnection = $this->_getDbConnection();
        $sql = 'SELECT * from users';
        $results = $dbConnection->query($sql);
        foreach($results as $row) {
            $users[] = $this->_loadUser($row);
        }
        return $users;
    }

    public function getUserByMail($mail) {
        $dbConnection = $this->_getDbConnection();
        $sql = 'SELECT * from users WHERE mail = :mail';
        $statement = $dbConnection->prepare($sql);
        $statement->bindParam(':mail', $mail, PDO::PARAM_STR);
        $statement->execute();
        $row = $statement->fetch();
        return $this->_loadUser($row);
    }

    public function getUserById($id) {
        $dbConnection = $this->_getDbConnection();
        $sql = 'SELECT * from users WHERE id_user = :id';
        $statement = $dbConnection->prepare($sql);
        $statement->bindParam(':id', $id, PDO::PARAM_INT);
        $statement->execute();
        $row = $statement->fetch();
        return $this->_loadUser($row);
    }

}


