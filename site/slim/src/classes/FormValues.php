<?php

class FormValues {

    public static function initialize() {
        if (!isset($_SESSION['values'])) {
            self::reset();
        }
    }

    public static function addValue($value, $key) {
        $_SESSION['values'][$key] = $value;
        
    }

    public static function reset() {
        $_SESSION['values'] = [];
    }

    public static function getValues() {
        return $_SESSION['values'];
    }

}