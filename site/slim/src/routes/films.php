<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/films', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'films.phtml', $args);
});

$app->get('/api/films/1', function (Request $request, Response $response, array $args) {
    $data = [
            ['title' => 'Pulp fiction'],
            ['title' => 'Star Wars'],
            ['title' => 'Star Trek']
        ];
    return $response->withJson($data);
});


$app->get('/api/films/2', function (Request $request, Response $response, array $args) {
    $data = [
            ['title' => 'Big Lebowski'],
            ['title' => 'Star Trek 2'],
        ];
    return $response->withJson($data);
});