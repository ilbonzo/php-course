<?php

use Slim\Http\Request;
use Slim\Http\Response;

$app->post('/users/login', function (Request $request, Response $response) {
    $data = $request->getParsedBody();
    $result = false;
    $userCollection = new UserCollection();
    $user = $userCollection->getUserByMail($data['email']);
    if ($user !== false) {
        if ($user->getPassword() === $data['password']) {
            $result = true;
        }
    }

    if ($result) {
        $session = new Session();
        $session->login($data);
    }
    print 'ERRORE';
    die;
});

$app->get('/users/logout', function (Request $request, Response $response, array $args) {
    $session = new Session();
    $session->logout();
    $session->redirectIfNotLogged();
});

$app->get('/users', function (Request $request, Response $response, array $args) {
    $session = new Session();
    $session->redirectIfNotLogged();

    $userCollection = new UserCollection();
    $args['allUsers'] = $userCollection->getUsers();
    return $this->renderer->render($response, 'users_table.phtml', $args);
});

$app->get('/users/edit/{id}', function (Request $request, Response $response, array $args) {
    $session = new Session();
    $session->redirectIfNotLogged();

    $userCollection = new UserCollection();
    $args['user'] = $userCollection->getUserById($args['id']);

    $args['errorMessages'] = ErrorMessages::getMessages();
    ErrorMessages::reset();

    return $this->renderer->render($response, 'user_form.phtml', $args);
});

$app->post('/users/update', function (Request $request, Response $response) {
    $session = new Session();
    $session->redirectIfNotLogged();

    $data = $request->getParsedBody();
    $userCollection = new UserCollection();
    $user = $userCollection->getUserById($data['id']);
    $user->setEmail($data['email']);
    $user->setPassword($data['password']);
    $user->setName($data['name']);
    $user->setSurname($data['surname']);
    if ($user->update()) {
        header('Location: /slim/public/users');
        exit();
    }
    print 'error';
    die;
});

$app->get('/users/delete/{id}', function (Request $request, Response $response, array $args) {
    $session = new Session();
    $session->redirectIfNotLogged();

    $userCollection = new UserCollection();
    $user = $userCollection->getUserById($args['id']);
    if ($user->delete()) {
        header('Location: /slim/public/users');
        exit();
    }
    print 'error';
    die;
});

$app->get('/users/create', function (Request $request, Response $response, array $args) {
    $session = new Session();
    $session->redirectIfNotLogged();

    $args['errorMessages'] = ErrorMessages::getMessages();
    ErrorMessages::reset();
    $args['values'] = FormValues::getValues();
    FormValues::reset();
    return $this->renderer->render($response, 'user_form.phtml', $args);
});

$app->post('/users/insert', function (Request $request, Response $response) {
    $session = new Session();
    $session->redirectIfNotLogged();
    
    $data = $request->getParsedBody();

    $user = new User($data['email'], $data['password']);
    $user->setName($data['name']);
    $user->setSurname($data['surname']);

    $userValidator = new UserValidator();
    $result = $userValidator->isValid($user);
    if ($result === true) {
        if ($user->insert()) {
            header('Location: /slim/public/users');
            exit();
        }
    }
    foreach($result as $m) {
        ErrorMessages::addMessage($m);
    }
    foreach($data as $key => $d) {
        FormValues::addValue($d, $key);
    }
    header('Location: /slim/public/users/create');
    die;
});