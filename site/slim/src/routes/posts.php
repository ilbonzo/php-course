<?php
use Slim\Http\Request;
use Slim\Http\Response;

$app->get('/posts', function (Request $request, Response $response, array $args) {
    return $this->renderer->render($response, 'posts.phtml', $args);
});

// form modifica post
$app->get('/posts/edit/{id}', function (Request $request, Response $response, array $args) {
});

// action di modifica post
$app->post('/posts/update', function (Request $request, Response $response) {
});

// action di delete
$app->get('/posts/delete/{id}', function (Request $request, Response $response, array $args) {
});

// form di creazione post
$app->get('/posts/create', function (Request $request, Response $response, array $args) {
});

// action di inserimento post
$app->post('/posts/insert', function (Request $request, Response $response) {
});