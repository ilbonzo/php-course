<?php
require_once('bootstrap.php');

$userCollection = new UserCollection();
$allUsers = $userCollection->getUsers();


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    
    <div class="container">

<?php
require_once('menu.php');
?>
        <ul>
<?php
    foreach ($allUsers as $user) {
?>
        <li>
            <?php print $user->getEmail(); ?>
             - <a href="user_update_form.php?id=<?php print $user->getId() ?>">modifica</a>
             - <a href="user_delete.php?id=<?php print $user->getId() ?>">elimina</a>
        </li>
<?php
    }
?>
        </ul>
    </div>

</body>
</html>