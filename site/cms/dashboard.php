<?php
require_once('bootstrap.php');
require_once('verify_session.php');
redirectIfNotLogged();

// instanzio oggetto della classe User
// $user = new User($_SESSION['email'], $_SESSION['password']);

$userCollection = new UserCollection();
$allUsers = $userCollection->getUsers();

$user = $userCollection->getUserByMail($_SESSION['email']);


?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    
    <div class="container">

<?php
require_once('menu.php');
?>

        <p>Bravo <?php print $user->getEmail(); ?>  hai fatto login</p>
        <p>La tua password è <em><?php print $user->getEncryptedPassword(); ?></em></p>

        <?php 
        var_export($allUsers);
        ?>
    </div>

</body>
</html>