<?php
require_once('bootstrap.php');

$result = false;
$userCollection = new UserCollection();
$user = $userCollection->getUserByMail($_POST['email']);
if ($user->getPassword() === $_POST['password']) {
    $result = true;
}

session_start();
if ($result) {
    $_SESSION['logged'] = true;
    $_SESSION['loginDate'] = time();
    $_SESSION['email'] = $_POST['email'];
    $_SESSION['password'] = $_POST['password'];
    header('Location: /cms/dashboard.php');
    exit();
}
$_SESSION['logged'] = false;
header('Location: /cms/form.php');
exit();



