<?php
require_once('bootstrap.php');

$userCollection = new UserCollection();

$user = $userCollection->getUserById($_POST['id']);

$user->setEmail($_POST['email']);
$user->setPassword($_POST['password']);
$user->setName($_POST['name']);
$user->setSurname($_POST['surname']);

if ($user->update()) {
    header('Location: /cms/users_table.php');
    exit();
}
print 'ERRORE';