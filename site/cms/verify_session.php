<?php
session_start();

function isLogged() {
    if ($_SESSION['logged'] && !sessionExpired($_SESSION['loginDate'])) {
        return true;
    }
    logout();
    return false;
}

function sessionExpired($time) {
    // $timeToExpire = $time + (3600 * 24);
    $timeToExpire = $time + (30);
    $now = time();
    if ($now > $timeToExpire) {
        return true;
    }
    return false;
}

function logout() {
    $_SESSION['logged'] = false;
    $_SESSION['loginDate'] = null;
}


function redirectIfNotLogged() {
    if (!isLogged()) {
        header('Location: /cms/form.php');
        exit();
    }
}
