<?php
require_once('bootstrap.php');

$userCollection = new UserCollection();

$user = $userCollection->getUserById($_GET['id']);

if ($user->delete()) {
    header('Location: /cms/users_table.php');
    exit();
}
print 'ERRORE';