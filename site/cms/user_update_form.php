<?php
require_once('bootstrap.php');

$userCollection = new UserCollection();

$user = $userCollection->getUserById($_GET['id']);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Page Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
<body>
    <div class="container">

<?php
require_once('menu.php');
?>
        
        <form method="POST" action="/cms/user_update.php">
            <input type="hidden" name="id" value="<?php print $user->getId() ?>" />
            <div class="form-group">
                <label for="exampleInputEmail1">Email address</label>
                <input type="email" name="email" class="form-control" id="exampleInputEmail1" aria-describedby="emailHelp" placeholder="Enter email" value="<?php print $user->getEmail() ?>">
                <small id="emailHelp" class="form-text text-muted">We'll never share your email with anyone else.</small>
            </div>
            <div class="form-group">
                <label for="exampleInputPassword1">Password</label>
                <input type="password" name="password" class="form-control" id="exampleInputPassword1" placeholder="Password" value="<?php print $user->getPassword() ?>">
            </div>
            <div class="form-group">
                <label for="exampleInputName1">Name</label>
                <input type="text" name="name" class="form-control" id="exampleInputName1" aria-describedby="nameHelp" placeholder="Enter name" value="<?php print $user->getName() ?>">
            </div>
            <div class="form-group">
                <label for="exampleInputSurname1">Surname</label>
                <input type="text" name="surname" class="form-control" id="exampleInputSurname1" aria-describedby="surnameHelp" placeholder="Enter surname" value="<?php print $user->getSurname() ?>">
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</body>
</html>