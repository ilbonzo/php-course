<nav>
    <ul class="nav">
        <li class="nav-item">
            <a class="nav-link active" href="/cms/dashboard.php">Dashboard</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cms/logout.php">Logout</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cms/public_posts.php">Posts pubblici</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cms/private_posts.php">Posts privati</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" href="/cms/users_table.php">Utenti</a>
        </li>
    </ul>
</nav>