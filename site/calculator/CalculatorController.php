<?php
require_once('Calculator.php');

class CalculatorController {
    
    private $_operation;
    
    private $_input1;
    
    private $_input2;

    private $_result = 0;

    public function __construct(array $post) {
        $this->_input1 = isset($post['input1']) ? $post['input1'] : 0;
        $this->_input2 = isset($post['input2']) ? $post['input2'] : 0;
        $this->_operation = isset($post['operation']) ? $post['operation'] : 'sum';
    }

    public function getInput1() {
        return $this->_input1;
    }

    public function getInput2() {
        return $this->_input2;
    }

    public function getOperation() {
        return $this->_operation;
    }

    public function doOperation() {
        $calculator = new Calculator();
        if ($this->_operation === 'sum') {
            $this->_result = $calculator->sum($this->_input1, $this->_input2); 
        }
        
        if ($this->_operation === 'diff') {
            $this->_result = $calculator->diff($this->_input1, $this->_input2); 
        }
    }

    public function getResult() {
        $this->doOperation();
        return $this->_result;
    }

}
