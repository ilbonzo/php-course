<?php
require_once('CalculatorController.php');
$calculatorController = new CalculatorController($_POST);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Simple calculator</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
</head>
    <body>

        <div class="container">
            <div> 
                Result: <?php print $calculatorController->getResult(); ?>
            </div>
            <form method="POST" action="index.php">
                <div class="form-group">
                    <label for="data-input1">Number 1</label>
                    <input type="number" name="input1" 
                        class="form-control" id="data-input1" 
                        placeholder="insert number"
                        value="<?php print $calculatorController->getInput1(); ?>">
                    <label for="data-input2">Number 2</label>
                    <input type="number" name="input2" 
                        class="form-control" id="data-input2" 
                        placeholder="insert number"
                        value="<?php print $calculatorController->getInput2(); ?>"">
                    <small id="number-help" class="form-text text-muted">Insert number and choose operation</small>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="operation" id="sum" value="sum" checked>
                        <label class="form-check-label" for="operation">
                            Sum
                        </label>
                    </div>
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="operation" id="diff" value="diff">
                        <label class="form-check-label" for="operation">
                            Diff
                        </label>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </body>
</html>