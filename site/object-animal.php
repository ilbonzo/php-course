<?php

class Animal {

    private $_type;

    private $_race = null;

    public function __construct($type) {
        $this->_type = $type;
    }

    public function getType() {
        return $this->_type;
    }

    public function getRace() {
        return $this->_race;
    }

    public function setRace($race) {
        $this->_race = $race;
    }

    public function speak() {
        if ($this->_type === 'cane') {
            print 'bau';
        }
        if ($this->_type === 'gatto') {
            print 'miao';
        }
        if ($this->_type === 'topo') {
            print 'squit';
        }
        print '<br/>';
    }

    public function equals(Animal $animal) {
        if ($this->getType() === $animal->getType() && $this->getRace() === $animal->getRace()) {
            return true;
        }
        return false;
    }
  
}

$dog = new Animal('cane');
$cat = new Animal('gatto');
$dog2 = new Animal('cane');
$animal1 = new Animal('topo');


$dog->speak();
$cat->speak();
$dog2->speak();
$animal1->speak();

if ($dog->getType() === $cat->getType()) {
    print 'sono tipo uguale';
} else {
    print 'sono tipo diverso';
}

print '<br/>';
if ($dog->equals($cat)) {
    print 'uguali';
} else {
    print 'diversi';
}
print '<br/>';

$dog->setRace('labrador');
$dog2->setRace('alano');

if ($dog->equals($dog2)) {
    print 'uguali';
} else {
    print 'diversi';
}
print '<br/>';

// ERROR
// print $dog->equals('pippo');